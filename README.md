# print-request

HTTPリクエストの内容を確認するための検証用Webサーバー。

- リクエスト内容が文字列で返ってくる
- リクエスト内容をログに出力する

## 使い方

オプション無しで実行すると、ポート`8080`で待ち受けるWebサーバーが起動する。

```sh
./print-request
```

利用可能なオプションは`-h`で確認可能。

```console
./print-request -h

Usage of print-request:
  -bind string
      Bind address (default ":8080")
  -body
      Dump HTTP body
  -cors
      Enable CORS (default true)
  -log-request-dump
      Logging request dump
```

- `-bind`: バインドするアドレスを指定
  - 例: `-bind :3000`, `-bind 127.0.0.1:8080`
- `-body`: `POST`等のリクエストボディもダンプに含める
- `-cors`: CORSを有効にする（デフォルトで有効）
  - 無効にする場合は`-cors=false`
- `-log-request-dump`: サーバー側のログ出力にリクエストダンプも含める
  - デフォルトではサーバー側のログは、リモートアドレス、プロトコル、HTTPメソッド、URLのみ

## 出力イメージ

### クライアント側のレスポンス例

```console
> curl -d "id=123" "http://localhost:8080/foo/bar?key=value"
POST /foo/bar?key=value HTTP/1.1
Host: localhost:8080
Accept: */*
Content-Length: 6
Content-Type: application/x-www-form-urlencoded
User-Agent: curl/7.79.1

id=123
```

### サーバー側のログ出力例

```console
> ./print-request --body --log-request-dump
Start and listen on :8080
2022/07/07 16:43:53 127.0.0.1:61492 HTTP/1.1 POST /foo/bar?key=value
POST /foo/bar?key=value HTTP/1.1
Host: localhost:8080
Accept: */*
Content-Length: 6
Content-Type: application/x-www-form-urlencoded
User-Agent: curl/7.79.1

id=123
```
