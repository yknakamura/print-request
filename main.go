package main

import (
	"flag"
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
	"os"
	"strconv"
	"strings"
)

var (
	addr           = flag.String("bind", ":8080", "Bind address")
	enableCORS     = flag.Bool("cors", true, "Enable CORS")
	dumpBody       = flag.Bool("body", false, "Dump HTTP body")
	loggingReqDump = flag.Bool("log-request-dump", false, "Logging request dump")
)

var (
	allowedMethods = []string{
		http.MethodHead,
		http.MethodGet,
		http.MethodPost,
		http.MethodPut,
		http.MethodPatch,
		http.MethodDelete,
		http.MethodOptions,
	}
)

func dumpRequest(r *http.Request, dumpBody bool) (dump []byte, logContent string, err error) {
	dump, err = httputil.DumpRequest(r, dumpBody)
	if err != nil {
		return nil, "", err
	}

	headers := getLoggingRequestHeader(r)

	if *loggingReqDump {
		logContent = headers + "\n" + string(dump)
	} else {
		logContent = headers
	}

	return dump, logContent, nil
}

func getLoggingRequestHeader(r *http.Request) string {
	return strings.Join([]string{
		r.RemoteAddr,
		r.Proto,
		r.Method,
		r.RequestURI,
	}, " ")
}

func requestDumpHandler(w http.ResponseWriter, r *http.Request) {
	dump, logContent, err := dumpRequest(r, *dumpBody)
	if err != nil {
		log.Println("DumpRequest error:", err)
	}
	log.Println(logContent)

	fmt.Fprintln(w, string(dump))
}

func cors(h http.HandlerFunc) http.HandlerFunc {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if isPreflightRequest(r) {
			log.Println("Preflight request")

			origin := r.Header.Get("Origin")
			if origin == "" {
				log.Println("Preflight aborted: No origin header")
				w.WriteHeader(http.StatusNoContent)
				return
			}

			_, logContent, err := dumpRequest(r, *dumpBody)
			if err != nil {
				log.Println("DumpRequest error:", err)
				w.WriteHeader(http.StatusNoContent)
				return
			}
			log.Println(logContent)

			w.Header().Set("Access-Control-Allow-Origin", origin)
			w.Header().Set("Vary", "Origin")
			w.Header().Set("Access-Control-Allow-Methods", strings.Join(allowedMethods, ", "))
			w.Header().Set("Access-Control-Allow-Headers", "*")
			w.WriteHeader(http.StatusNoContent)
		} else {
			origin := r.Header.Get("Origin")
			if origin == "" {
				origin = "*"
			}
			w.Header().Set("Access-Control-Allow-Origin", origin)
			w.Header().Set("Vary", "Origin")
			w.Header().Set("Access-Control-Expose-Headers", "*")

			h.ServeHTTP(w, r)
		}
	})
}

func isPreflightRequest(r *http.Request) bool {
	return r.Method == http.MethodOptions && r.Header.Get("Access-Control-Request-Method") != ""
}

func isIntegerString(value string) bool {
	_, err := strconv.Atoi(value)
	return err == nil
}

func main() {
	flag.Parse()
	bindAddr := *addr

	if !strings.Contains(bindAddr, ":") {
		if isIntegerString(bindAddr) {
			bindAddr = ":" + bindAddr
		} else {
			fmt.Fprintln(os.Stderr, "Bind address must contain port number.")
		}
	}

	handler := requestDumpHandler
	if *enableCORS {
		handler = cors(requestDumpHandler)
	}
	http.HandleFunc("/", handler)

	fmt.Printf("Start and listen on %s\n", bindAddr)
	err := http.ListenAndServe(bindAddr, nil)
	if err != nil {
		fmt.Fprintln(os.Stderr, err)
	}
}
