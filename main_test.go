package main

import "testing"

func TestIsIntegerString(t *testing.T) {
	tests := []struct {
		value    string
		expected bool
	}{
		{value: "1", expected: true},
		{value: "3000", expected: true},
		{value: "-1", expected: true},
		{value: "0", expected: true},
		{value: "a", expected: false},
		{value: "1a", expected: false},
		{value: "a1", expected: false},
		{value: " 1", expected: false},
		{value: "", expected: false},
	}

	for _, tt := range tests {
		t.Run(tt.value, func(t *testing.T) {
			got := isIntegerString(tt.value)
			if got != tt.expected {
				t.Errorf("got %v, want %v", got, tt.expected)
			}
		})
	}
}
